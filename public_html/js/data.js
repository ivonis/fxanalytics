/* global DATE_TIME_FORMAT, SERVICE_URI, PAGE_SIZE */


SPREAD_MIN_URL = SERVICE_URI + "/spread_min";
SPREAD_AVG_URL = SERVICE_URI + "/spread";
SPREAD_MAX_URL = SERVICE_URI + "/spread_max";


Range = {
    m1: 1,
    h1: 2,
    d1: 3,
    format: function(date, range) {
        switch (range) {
            case this.m1 : 
                return date.format("HH:MM");
            case this.h1 : 
                return date.format("d HH:MM");
            case this.d1 : 
                return date.format("mmm d");
            default : 
                return date.toDateString();
        }
    },
    incTime: function(date, range, n) {
        if (n !== 0) {
            n = n || 1;
        }
        switch (range) {
            case this.m1 :
                date.setMinutes(date.getMinutes() + n);
                break;
            case this.h1 : 
                date.setHours(date.getHours() + n);
                break;
            case this.d1 : 
                date.setDate(date.getDate() + n);
                break;
        }
        return date;
    }
};

function TimeRange(range, from, to) {
    this.range = range || Range.d1;
    if (!from) {
        this.to = to || new Date();
        this.from = this.get(-PAGE_SIZE + 1, new Date(this.to.getTime()));
    } else {
        this.from = from;
        this.to = to || this.get(PAGE_SIZE - 1, new Date(from.getTime()));
    }
    this.reset();
};

TimeRange.prototype.get = function(ind, from) {
    from = from || new Date(this.from.getTime());
    return Range.incTime(from, this.range, ind);
};

TimeRange.prototype.next = function(step) {
    step = step || 1;
    if (this.current.getTime() > this.to.getTime()) {
        return null;
    }
    var time = new Date(this.current.getTime());
    this.current = this.get(step, this.current);
    return time;
};

TimeRange.prototype.reset = function() {
    this.current = new Date(this.from.getTime());
    this.first = true;
    return this;
};

TimeRange.prototype.shift = function(fromDelta, toDelta) {
    fromDelta = fromDelta || 0;
    toDelta = toDelta || 0;
    return new TimeRange(this.range, this.get(fromDelta), this.get(toDelta, this.to));
};

// -------------------------------------------------------------

function loadSpread3(params, fxData) {
    var range = Range[params.range];
    var to = params.to ? new Date(params.to.getTime()) : new Date();
    var tr = new TimeRange(range, params.from, to);
    
//    alert(
//        "Local:" + params.from.format(DATE_TIME_FORMAT) + " to " + to.format(DATE_TIME_FORMAT)
//        + "\nUTC:" + params.from.format(DATE_TIME_FORMAT, true) + " to " + to.format(DATE_TIME_FORMAT, true)
//    );
    
    var qp = "flow=" + params.flow 
        + "&symbol=" + params.symbol 
        + "&range=" + params.range 
        + "&from=" + params.from.format(DATE_TIME_FORMAT, true)
        + "&to=" + to.format(DATE_TIME_FORMAT, true);

    $.when(
        $.getJSON(SPREAD_MIN_URL, qp),
        $.getJSON(SPREAD_AVG_URL, qp),
        $.getJSON(SPREAD_MAX_URL, qp)
    ).done(function(r1, r2, r3){
        var data = [];
        if (!r1[0].data || !r2[0].data || !r3[0].data) {
            fxData.setData({data: [], tr: tr});
            return;
        }
        $.each(r1[0].data, function(key, val){
            if (val) {
                var d = [val];
                data[key] = d;
            } else {
                data[key] = null;
            }
        });
        $.each(r2[0].data, function(key, val){
            if (data[key] && val) {
                data[key][1] = val;
            } else {
                data[key] = null;
            }
        });
        $.each(r3[0].data, function(key, val){
            if (data[key] && val) {
                data[key][2] = val;
            } else {
                data[key] = null;
            }
        });
        fxData.setData({data: data, tr: tr});
    }).fail(function(e){
        alert("Loadin failed:" + e);
        fxData.setData({data: [], tr: tr});
    });
};
// -------------------------------------------------------------

function loadSpread3stat(params, fxData) {
    var range = Range[params.range];
    var to = params.to ? new Date(params.to.getTime()) : new Date();
    var tr = new TimeRange(range, params.from, to);
    var data = [];
    var m = 0.01;
    while(tr.next()) {
        var v = Math.random();
        while(v > 0.7 || v < 0.3) v = Math.random();
        var v1 = parseFloat(v.toFixed(6));
        v = Math.random();
        while(v < 0.1) v = Math.random();
        var v0 = parseFloat((v1 - Math.floor(v * 10) * m).toFixed(6));
        v = Math.random();
        while(v < 0.1) v = Math.random();
        var v2 = parseFloat((v1 + Math.floor(Math.random() * 10) * m).toFixed(6));
        data.push([v0, v1, v2]);
    }
    fxData.setData({data: data, tr: tr.reset()});
};

function FxPage(book, offset) {
    this.defauilt = {
        dataLabels:[{data: 0.0, label: "0.0"}, {data: 0.5, label: "0.5"}, {data: 1.0, label: "1.0"}],
        upperLimit:1.0
    };
    this.book = book;
    this.hasData = false;
    this.dataOffset = offset || 0;
    this.times = [];
    this.labels = [];
    this.normData = [];
    this.normDataStep = 0;
    this.minNormData = 0;
    this.maxNormData = 0;
}

FxPage.prototype.initEmpty = function(tr, pageSize) {
    tr = tr || new TimeRange();
    pageSize = pageSize || PAGE_SIZE;
    
    this.dataLabels = this.defauilt.dataLabels;
    this.upperLimit = this.defauilt.upperLimit;
    var time = tr.next();
    this.times = [];
    this.labels = [];
    this.hasData = false;
    var i = 0;
    while(time && i < pageSize) {
        this.times[i] = time;
        this.labels[i++] = Range.format(time, tr.range);
        time = tr.next();
    }
    return this;
};

FxPage.prototype.getData = function(ind) {
    var dInd = ind + this.dataOffset;
    var data = this.book.data;
    if (dInd < data.length) {
        return data[dInd];
    }
    return [];
};

FxPage.prototype.setData = function(data, fromInd, len) {
    fromInd = fromInd || 0;
    var _data = data.data;
    len = len || PAGE_SIZE;
    this.times = [];
    this.labels = [];
    this.normData = [];
    this.dataInd = fromInd;
    var min = Number.MAX_VALUE;
    var max = Number.MIN_VALUE;
    var toInd = fromInd + len;
    this.hasData = false;
    this.dataOffset = fromInd;
    var tr = data.tr.shift(fromInd);
    for(var i = fromInd, time = tr.next(); time && i < toInd; i++, time = tr.next()) {
        var row = _data[i];
        //var time = tr.next();
        if (hasValue(row)) {
            this.hasData = true;
            for (var j = 0; j < row.length; j++) {
                var d = row[j];
                if (min > d) min = d;
                if (max < d) max = d;
            }
        }
        var nInd = i - fromInd;
        this.times[nInd] = time;
        this.labels[nInd] = Range.format(time, tr.range);
    } 
    if (this.hasData) {
        var scale = this.book.scale;
        var nData = normalizeData(_data, min, max, fromInd, len, scale);
        this.normData = nData.normData;
        this.dataLabels =  nData.dataLabels;
        this.upperLimit = nData.upperLimit;
    }
    return this;
};

FxPage.prototype.fullLabel = function(ind) {
    var t = this.times[ind] || new Date();
    return t.format("default");
};

function FxDataBook(paper, pageSize, scale) {
    this.paper = paper;
    this.pageSize = pageSize || PAGE_SIZE;
    this.scale = scale || 1;
    this.data = [];
    this.pages = [];
}

FxDataBook.prototype.setData = function(data) {
    this.data = data.data;
    this.timeRange = data.tr;
    this.pages = [];
    var dataInd = 0;
    var pageInd = 0;
    var maxPageInd = this.data.length > this.pageSize ? this.data.length - this.pageSize + 1 : 1;
    while(dataInd < maxPageInd) {
        var page = new FxPage(this);
        do {
            page.setData(data, dataInd++, this.pageSize);
        } while(!page.hasData && dataInd < maxPageInd)
        this.pages[pageInd++] = (page.hasData)? page : page.initEmpty(this.timeRange.reset(), this.pageSize);
    }
    this.newData = true;
    this.paper.drawGraphics(this);
    return this;
};

FxDataBook.prototype.initEmpty = function(tr) {
    this.pages = [new FxPage(this).initEmpty(tr, this.pageSize)];
    this.paper.drawGraphics(this);
    return this;
};

FxDataBook.prototype.page = function(num) {
    var page = this.pages[num];
    return page || [];
};

FxDataBook.prototype.size = function() {
    return this.pages.length;
};


