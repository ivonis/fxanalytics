/* global SERVICE_URI, G_WIDTH, G_HEIGHT, PAGE_SIZE, DATE_TIME_FORMAT, UI_DATE_FORMAT, FLOW */

State = {
    setFlow: function(flow) {
        this.flow = flow;
        return this;
    },
    setSymbol: function(symbol) {
        this.symbol = symbol;
        return this;
    },
    setRange: function(range) {
        this.range = range;
        return this;
    },
    setFrom: function(from) {
        var currentYear = this.from ? State.from.getFullYear() : 0;
        this.from = from;
        if (from) {
            if (currentYear !== State.from.getFullYear()) {
                State.loadSymbols();
            } else {
                $("#filter-submit-btn").button({disabled: false});
            }
        }
        return this;
    },
    setTo: function(to) {
        this.to = to;
        return this;
    },
    loadFlows: function () {
        function changeFlow(event, ui) {
            State.flow = ui.item.value; 
            State.loadSymbols();
        }
        var initFlow = function(result) {
            var flow = initSelect(selectByName('flow'), result);
            flow.selectmenu({change: changeFlow});
            changeFlow(null, {item: {value: flow.val()}});
        };
        if (typeof FLOW !== "undefined" && FLOW) {
            initFlow({data:[FLOW]});
        } else {
            $.getJSON(SERVICE_URI + '/list', initFlow);
        }
    },
    loadSymbols: function () {
        $("#filter-submit-btn").button({disabled: true});
        function changeSymbol(event, ui) {
            State.setSymbol(ui.item.value); 
        }
        if (State.from) {
            var data = "flow=" + State.flow + "/" + State.from.getFullYear();
            $.getJSON(SERVICE_URI + '/list', data, function(result) {
                var symbol = initSelect(selectByName('symbol'), result);
                symbol.selectmenu({change: changeSymbol, disabled: false}).selectmenu("refresh");
                changeSymbol(null, {item: {value: symbol.val()}});
                $("#filter-submit-btn").button({disabled: false});
            });
        }
    }
};

$(function(){
    State.loadFlows();//setFlow(FLOW);
    var paper = new Paper("g-container", "g-canvas");
    State.fxData = new FxDataBook(paper).initEmpty();
    
//    loadSpread3(
//        {
//            flow:"ads", symbol:"AUDCAD", range:"h1", 
//            from:new Date(2016,1,18,0,31,05,936),
//            to:new Date(2016,1,18,13,29,04,936)
//        }, 
//        State.fxData
//    );

    // Init controls
    selectByName('symbol').selectmenu({disabled: true});
    $("#select-range").selectmenu({change: changeRange});
    changeRange(null, {item: {value: $("#select-range").val()}});
    $("#filter-submit-btn").button({disabled: true}).click(applyFilter);

    initDatepickers();
    //$("#g-canvas").scroll();

});

function initSelect(sel, result) {
    var data = result.data;
    var html = "";
    var selected = "selected";
    for(var i in data) {
        html+="<option" 
                + (selected ? " selected='" + selected + "'": "") + ">" 
                + data[i] 
                + "</option>";
        selected = null;
    }
    sel.html(html);
    return sel;
}

function changeRange(event, ui) {
    State.setRange(ui.item.value);
}

function selectByName(name) {
    return $("#select-" + name); 
}

function initDatepickers() {
    var dateOpts = {
        changeYear:true,
        showOn: "both",
        dateFormat: UI_DATE_FORMAT,
        defaultDate: new Date()
    };
    var timeOpts = {
        defaultHour: 0, 
        defaultMinute: 0, 
        PM: false
    };
    
    var changeFrom = function(){State.setFrom(getDateTime("from"));};
    var changeTo = function(){State.setTo(getDateTime("to"));};
    $("#date-from").datepicker(dateOpts).change(changeFrom);
    $("#time-from").timePicker(timeOpts);
    $("#date-to").datepicker(dateOpts).change(changeTo);
    $("#time-to").timePicker(timeOpts);
}

function getDateTime(name) {
    var cd = $("#date-" + name).datepicker("getDate");
    if (cd) {
        var d = new Date(cd.getTime());
        var t = $("#time-" + name).data();
        d.setHours(t.hours + (t.pm_time ? 12 : 0));
        d.setMinutes(t.minutes);
        return d;
    }
    return null;
}

function applyFilter() {
    $("#filter-submit-btn").button({disabled: true});
    State.from = getDateTime("from");
    State.to = getDateTime("to");
    
    loadSpread3($.extend({}, State), State.fxData);
}
