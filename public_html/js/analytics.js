/* global Raphael, Range, State, PAGE_SIZE */
G_WIDTH = 1180;
G_HEIGHT = 450;
G_LEFT = 40;
G_BOTTOM = 20;
G_TOP = 20;
var G_COLOR = ["#0f0", "#00f", "#f00"];
var G_TITLE = ["Min", "Middle", "Max"];

Raphael.fn.drawGrid = function (x, y, w, h, dx, dataLabels, color) {
    color = color || "#000";
    var p = [];
    var ii = dataLabels.length;
    for (var i = 0; i < ii; i++) {
        var y0 = Math.round(h - y * dataLabels[i].data) - .5;
        p = p.concat(["M", Math.round(x) + .5, y0, "H", Math.round(x + w) + .5]);
        var lbl = this.text(8, y0, dataLabels[i].label).attr({font: '12px Helvetica, Arial', fill: "#fff"});
        var lx = x - lbl.getBBox().width - .5;
        lbl.attr({x: lx}).toBack();
    }
    var y1 = y * dataLabels[0].data, y2 = y * dataLabels[ii - 1].data; 
    for (var x0 = 0; x0 <= w; x0 += dx) {
        p = p.concat(["M", Math.round(x + x0) + .5, Math.round(h - y1) + .5, "V", Math.round(h - y2) + .5]);
    }

    return this.path(p.join(",")).attr({stroke: color});
};

function Paper(parentContainerId, containerId) {
    var R = Raphael(containerId, G_WIDTH, G_HEIGHT);
    this.containerId = containerId;
    this.paper = R;
    this.txt = {font: '12px Helvetica, Arial', fill: "#fff"},
    this.txt1 = {font: '10px Helvetica, Arial', fill: "#fff"};
    this.gridWidth = G_WIDTH - G_LEFT;
    this.gridHeight = G_HEIGHT - G_BOTTOM - G_TOP;
    this._scroll = new GScroll(parentContainerId, this);
}

Paper.prototype.drawGraphics = function(fxData, pageNum) {
    var _p = this;
    var R = _p.paper;
    pageNum = pageNum || 0;
    var page = fxData.page(pageNum);
    R.clear();
    var labels = page.labels;
    var data = page.normData;
    var hasData = page.hasData;
    // Draw
    var size = fxData.pageSize;
    if (fxData.newData) {
        if (fxData.size() > 1) {
            _p._scroll.init(fxData, pageNum);
        } else {
            _p._scroll.init();
        }
        fxData.newData = false; 
    }

    var X = _p.gridWidth / size;
    var Y = _p.gridHeight / page.upperLimit;
    var dataLabels = page.dataLabels;
    R.drawGrid(
        G_LEFT + X * .5 + .5, Y, 
        _p.gridWidth - X, G_HEIGHT - G_BOTTOM, 
        X, dataLabels, 
        "#000"
    );
    var path = R.set(),
        label = R.set(),
        lx = 0, ly = 0,
        is_label_visible = false,
        leave_timer,
        blanket = R.set();
    for(var i = 0; i < G_COLOR.length; i++) {
        path.push(R.path().attr({stroke: G_COLOR[i], "stroke-width": 2, "stroke-linejoin": "round"}));
    }
    var frame;
    if (hasData) {
        label.push(R.text(60, 12, G_TITLE[0] + ": 0.0000001").attr(_p.txt));
        label.push(R.text(60, 27, page.fullLabel(0)).attr(_p.txt1));
        label.hide();
        frame = R.popup(100, 100, label, "right").attr({fill: "#000", stroke: "#666", "stroke-width": 2, "fill-opacity": .7}).hide();
    }
    
    var pp = [], points = [];
    for (var i = 0, ii = labels.length; i < ii; i++) {
        var x = Math.round(G_LEFT + X * (i + .5));
        R.text(x, G_HEIGHT - 6, labels[i]).attr(_p.txt).toBack();
        
        if (!hasData) {
            continue;
        }
        
        var row = data[i];
        if (noValue(row)) {
            continue;
        }
        var pRow = [];
        for (var j = 0; j < row.length; j++) {
            var y = Math.round(G_HEIGHT - G_BOTTOM - Y * row[j]);
            var p = pp[j];
            if (hasValue(p)) {
                p.push(x);
                p.push(y);
            } else {
                p = ["M", x, y, "L", x, y];
                pp.push(p);
            }
            pRow.push({x: x, y: y, ind: i});
        }
        points.push(pRow);
    }
    for (var i = 0, ii = points.length; i < ii; i++) {
        var pRow = points[i];
        for (var j = 0; j < points[i].length; j++) {
            var x = points[i][j].x;
            var y = points[i][j].y;
            var ind = points[i][j].ind;
            var dot = R.circle(x, y, 3).attr({fill: "#333", stroke: G_COLOR[j], "stroke-width": 2});
            var p0 = i ? points[i - 1][j] : {};
            var p1 = i < ii - 1 ? points[i + 1][j] : {};
            var hp = buildHoverContur(10, p0.x, p0.y, x, y,  p1.x, p1.y);
            blanket.push(R.path().attr({path: hp, stroke: "none", fill: "#fff", opacity: 0}));
            var contur = blanket[blanket.length - 1];
            (function (p, x, y, data, clr, lbl, dot) {
                    contur.hover(function () {
                    p.attr({"stroke-width": 4});
                    clearTimeout(leave_timer);
                    var side = "right";
                    if (x + frame.getBBox().width > G_WIDTH) {
                        side = "left";
                    }
                    var ppp = R.popup(x, y, label, side, 1),
                        anim = Raphael.animation({
                            path: ppp.path,
                            transform: ["t", ppp.dx, ppp.dy]
                        }, 200 * is_label_visible);
                    lx = label[0].transform()[0][1] + ppp.dx;
                    ly = label[0].transform()[0][2] + ppp.dy;
                    frame.show().stop().animate(anim);
                    label[0].attr({text: data, fill: clr}).show().stop().animateWith(frame, anim, {transform: ["t", lx, ly]}, 200 * is_label_visible);
                    label[1].attr({text: lbl}).show().stop().animateWith(frame, anim, {transform: ["t", lx, ly]}, 200 * is_label_visible);
                    dot.attr("r", 5);
                    is_label_visible = true;
                }, function () {
                    dot.attr("r", 3);
                    leave_timer = setTimeout(function () {
                        p.attr({"stroke-width": 2});
                        frame.hide();
                        label[0].hide();
                        label[1].hide();
                        is_label_visible = false;
                    }, 1);
                });
            })(path[j], x, y, G_TITLE[j] + ": " + page.getData(ind)[j], G_COLOR[j], page.fullLabel(ind), dot);
        }
    }
    if (!hasData) {
        $("#filter-submit-btn").button({disabled: noValue(State.from)});
        return;
    }
    for (var i = 0 ; i < pp.length; i++) {
        path[i].attr({path: pp[i]});
    }
    frame.toFront();
    label[0].toFront();
    label[1].toFront();
    blanket.toFront();
    $("#filter-submit-btn").button({disabled: noValue(State.from)});
};

function buildHoverContur(h, x0, y0, x, y, x1, y1) {
    if (noValue(x0)) {
        var xx1 = (x + x1) * .5 - .5;
        var yy1 = (y + y1) * .5 - .5;
        return ["M", x - 5, y - h, "V", y + h, "L", xx1, yy1 + h, "V", yy1 - h, "z"];
    } else if (noValue(x1)) {
        var xx0 = (x + x0) * .5 + .5;
        var yy0 = (y + y0) * .5 + .5;
        return ["M", x + 5, y - h, "V", y + h, "L", xx0, yy0 + h, "V", yy0 - h, "z"];
    }
    var xx0 = (x + x0) * .5 + .5;
    var yy0 = (y + y0) * .5 + .5;
    var xx1 = (x + x1) * .5 - .5;
    var yy1 = (y + y1) * .5 - .5;
    return ["M", xx0, yy0 - h, "V", yy0 + h, "L", x, y + h, xx1, yy1 + h, "V", yy1 - h,"L", x, y - h, "z"];
}

function GScroll(containerId, paper) {
    this.containerId = containerId;
    this.paper = paper;
    this.scrollBarId = containerId + '-scroll-bar'; 
    this.scrollId = containerId + '-scroll'; 
    var scrollBar = '<div id="' + this.scrollBarId + '">'
        + '<span class="scroll-box left-arrow">&#9668;</span>'
        + '<div class="scroll-box" id="' + this.scrollId + '"></div>'
        + '<span class="scroll-box right-arrow">&#9658;</span>'
        +'</div>';
    $(scrollBar).css({"width": (G_WIDTH - G_LEFT) + "px", "margin-left": G_LEFT + "px"})
        .appendTo($('#' + containerId));
}

GScroll.prototype.init = function(fxData, pos) {
    var self = this;
    pos = pos || 0;
    var scroll = $('#' + this.scrollId);
    function change(event, ui) {
        if(self.pageNum !== ui.value) {
            self.pageNum = ui.value;
            self.paper.drawGraphics(fxData, ui.value);
        }
    }
    scroll.css({"width": (G_WIDTH - G_LEFT - 100) + "px", "margin": "0 8px"})
        .slider({max: fxData.size() - 1, value: pos, slide: change, change: change});

    function scrollMove(delta) {
        if (delta) {
            var val = scroll.slider("option", "value");
            var max = scroll.slider( "option", "max" );
            var newVal = delta + val;
            if (newVal < 0){ newVal = 0;} else if (newVal > max) {newVal = max;}
            if (newVal !== val) {
                scroll.slider("option", "value", newVal);
            }
        }
    }
    function _scrollMove(event) {
        var data = event.data;
        scrollMove((data && data.delta) ? data.delta : 0);
    }
    $(".left-arrow").click({delta: -1}, _scrollMove);
    $(".right-arrow").click({delta: 1}, _scrollMove);
    var fieldId = this.paper.containerId;
    var kMove = this.paper.gridWidth / fxData.pageSize;
    this.mouseObserver = new MouseObserver(fieldId, kMove, scrollMove);
};

function MouseObserver(fieldId, kMove, moveFn) {
    var self = this;
    this.fieldId = fieldId;
    this.mDown = {};
    this.mMove = {};
    this.mUp = {};
    this.kMove = kMove || 1;
    var field =  $("#" + fieldId);
    field.mousedown(function(event){
        if (event.which === 1) {
            self.mDown.x = event.pageX;
        }
    });
    field.mousedown(function(event){
        if (event.which === 1) {
            self.mDown.x = event.pageX;
        }
    });
    field.mousemove(function(event){
        if (self.mDown.x) {
            self.mMove.x = event.pageX;
            if (self.mMove.x !== self.mDown.x) {
                var delta = Math.round((self.mDown.x - self.mMove.x)/self.kMove);
                if (delta !== 0) {
                    moveFn(delta);
                    self.mDown.x = self.mMove.x;
                }
            }
        }
    });
    function moveStop() {
        self.mDown.x = 0;
        self.mMove.x = 0;
    }
    field.mouseup(moveStop).mouseleave(moveStop);
}

