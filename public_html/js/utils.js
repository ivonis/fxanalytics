
function normalizeData(data, min, max, i0, len, scale) {
    i0 = i0 || 0;
    len = len || data.length;
    scale = scale || 1;
    var ii = len + i0;
    var d = max - min;
    var isConst = (d < 1.0e-10);
    if (isConst) {
        d = max * 10 - min;
    }
    var n = Math.floor(Math.log(d)/Math.LN10); //lg(d)
    if (isConst) {
        n = n - 1; // поправка для прямой
    }
    var mDelta = Math.pow(10, n); // маска для наибольшего изменяемого разряда
    var mNorm = Math.pow(10, -n - 1); // нормализационный множитель
    var lowLimit = min > mDelta ? Math.floor(min / mDelta - 1) * mDelta : 0;
    var nData = []; 
    for (var i = i0; i < ii; i++) {
        var row = data[i];
        if (hasValue(row)) {
            var nRow = [];
            for (var j = 0; j < row.length; j++) {
                var v = row[j];
                nRow[j] = hasValue(v) ?(v - lowLimit) * mNorm * scale : v;
            }
            nData[i - i0] = nRow;
        } else {
            nData[i] = null;
        }
    }
    var dataLabels = [];
    var upperLimit = Math.floor(max / mDelta + 1) * mDelta;
    for (var val = lowLimit; val <= upperLimit; val += mDelta) {
        dataLabels.push(
            {
                data: (val - lowLimit) * mNorm * scale, 
                label: val.toFixed(Math.abs(n) + 1)
            }
        );
    }
    
    return {
        normData: nData,
        dataLabels: dataLabels,
        upperLimit: (upperLimit - lowLimit) * mNorm * scale
    };
}

function hasValue(x) {
    return x !== undefined && x !== null;
}

function noValue(x) {
    return x === undefined || x === null;
}




